{-# LANGUAGE OverloadedStrings #-}

module ALPS.Graph.Render(
  renderSettings,
  makeEvents,
  renderTokens,
  step
  ) where

import Control.Monad (void)
import Data.Monoid   (mconcat, mempty, (<>))

import Conduit      (MonadThrow, PrimMonad, evalStateC, leftover)
import Data.Conduit (ConduitT, await, yield, (.|))

import Control.Monad.State.Strict (StateT, get, put)

import Data.Text (Text, pack)

import Data.XML.Types         (Content (ContentText), Event (..), Name)
import Text.XML.Stream.Render (RenderSettings (..), def, renderText)

import ALPS.Graph.Token

attr :: Name -> Text -> [(Name, [Content])]
attr name text = [(name, [ContentText text])]

-- rendering
renderSettings :: RenderSettings
renderSettings = def { rsPretty = True, rsXMLDeclaration = False }

makeEvents :: Monad m => ConduitT Token Event m (Maybe (Error Text))
makeEvents = evalStateC SEmpty step

renderTokens :: (PrimMonad m, MonadThrow m) => ConduitT Token Text m ()
renderTokens = void makeEvents .| renderText renderSettings

openTag = EventBeginElement
closeTag = EventEndElement

tellError token state
  = pure $ Just $ Error state $ "Wrong token: " <> (pack $ show token)

earlyEndError state
  = pure $ Just $ Error state "Unexpected end of stream!"

step :: Monad m => ConduitT Token Event (StateT StreamState m) (Maybe (Error Text))
step = do
  state <- get
  case state of
    SEmpty -> do
      yield $ openTag "LATTICES" []
      put SGraph
      step

    SGraph -> do
      token <- await
      case token of
        Just (TokenHeader (GraphHeader name vertices edges)) -> do
          let edgeAttr = case edges of
                Just edges' -> attr "edges" edges'
                _           -> mempty
              attrs = mconcat
                [ attr "vertices" vertices
                , attr "name" name
                , edgeAttr ]
          yield $ openTag "GRAPH" attrs
          put SEdges
          step
        Just token ->
          tellError token state
        Nothing -> do
          pure Nothing

    SEdges -> do
      token <- await
      case token of
        Just (TokenEdge (Edge ty from to)) -> do
          let attrs = mconcat
                [ attr "source" from
                , attr "target" to
                , typeAttr ]
              typeAttr = case ty of
                Just ty' -> attr "type" ty'
                _        -> mempty
          yield $ openTag "EDGE" attrs
          yield $ closeTag "EDGE"
          put SEdges
          step

        Just (t@(TokenHeader _)) -> do
          yield $ closeTag "GRAPH"
          leftover t -- this should feed 'step', but it doesn't!
          put SGraph
          step
        Just TokenEnd -> do
          yield $ closeTag "GRAPH"
          yield $ closeTag "LATTICES"
          pure Nothing
        Just token ->
          tellError token state
        Nothing ->
          earlyEndError state

    SError ->
      pure Nothing
