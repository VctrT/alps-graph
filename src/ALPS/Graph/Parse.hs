module ALPS.Graph.Parse(
  parseTokens,
  step
  ) where

import Control.Exception (SomeException)
import Unsafe.Coerce     (unsafeCoerce)

import Control.Monad.State.Strict (StateT, get, put)

import Conduit (ConduitT, awaitForever, evalStateC, execStateC, lift, yield)

import Data.Text (Text, pack)

import Data.XML.Types        (Content, Event (..), Name)
import Text.XML.Stream.Parse (AttrParser, attr, requireAttr)

import ALPS.Graph.Token

-- | Streaming parser for ALPS XML lattice library (finite lattices only)
parseTokens :: Monad m => ConduitT Event Token m ()
parseTokens = evalStateC SEmpty step

-- | Simple, exception-free state machine for parsing, made just for fun
step :: Monad m => ConduitT Event Token (StateT StreamState m) ()
step = awaitForever $ \event -> do
  state <- get
  step' state event

step' :: Monad m => StreamState -> Event -> ConduitT Event Token (StateT StreamState m) ()
step' SError _ = pure ()

step' SEmpty (EventBeginElement "LATTICES" []) = put SGraph
step' SGraph (EventEndElement "LATTICES") = yield TokenEnd

step' SGraph ev@(EventBeginElement "GRAPH" attrs) =
  let gh' = parseAttrs attrs $ do
        name <- requireAttr "name" -- TODO: handle both upper and lower cases?
        vertices <- requireAttr "vertices"
        edges <- attr "edges"
        pure $! GraphHeader name vertices edges
  in case gh' of
    Right gh -> do
      put SEdges
      yield $! TokenHeader gh
    Left e -> do
      put SError
      yield $! TokenError $! Error
        { _state = SGraph
        , _elt = pack $ show ev ++ ": " ++ show e }
step' SGraph (EventEndElement "GRAPH") = pure ()

step' SEdges ev@(EventBeginElement "EDGE" attrs) =
  let edge' = parseAttrs attrs $ do
        ty <- attr "type"
        src <- requireAttr "source"
        dest <- requireAttr "target"
        pure $! Edge ty src dest
  in case edge' of
       Right edge -> yield $! TokenEdge edge
       Left e -> do
         put SError
         yield $! TokenError $! Error
           { _state = SEdges
           , _elt = pack $ show ev ++ ": " ++ show e }
step' SEdges (EventEndElement "EDGE") = pure ()

step' SEdges (EventEndElement "GRAPH") = put SGraph

-- ignore comments, begin of document and other `Event`s
step' s _ = pure ()

wrongElt :: Monad m => Event -> ConduitT a Token (StateT StreamState m) ()
wrongElt e = do
  s <- lift $ get
  put SError
  yield $! TokenError $! Error
    { _state = s
    , _elt = pack $ show e }

-- it's a shame ofc
parseAttrs :: [(Name, [Content])] -> AttrParser a -> Either SomeException a
parseAttrs x p = snd <$> (unsafeCoerce p) x
