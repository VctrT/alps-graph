{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveTraversable #-}
module ALPS.Graph.Token where

import Data.Semigroup((<>))
import Data.Bifunctor

import Control.Lens
import Control.Lens.TH

import Data.Text (Text)
import Data.ByteString(ByteString)
import Data.Attoparsec.ByteString.Char8(parseOnly, decimal)
import qualified Data.Attoparsec.Text as T

import Control.Monad(void)
import Conduit

-- | State for streaming processing
data StreamState
  = SEmpty
  | SError
  | SGraph
  | SEdges
  deriving(Eq, Show)

makePrisms ''StreamState

data GraphHeader s n = GraphHeader
  { _graphName     :: !s
  , _graphVertices :: !n
  , _graphEdges    :: !(Maybe n)
  } deriving(Eq, Show, Functor, Foldable, Traversable)

instance Bifunctor GraphHeader where
  bimap f g (GraphHeader n v e) = GraphHeader (f n) (g v) (fmap g e)

makeLenses ''GraphHeader

data Edge s n = Edge
  { _edgeType :: !(Maybe s)
  , _edgeSrc  :: !n
  , _edgeDst  :: !n
  } deriving(Eq, Show, Functor, Foldable, Traversable)

instance Bifunctor Edge where
  bimap f g (Edge n s d) = Edge (fmap f n) (g s) (g d)

makeLenses ''Edge

data Vertex s n = Vertex
  { _vertexNo   :: !n
  , _vertexType :: !(Maybe s)
  } deriving(Eq, Show, Functor, Foldable, Traversable)

instance Bifunctor Vertex where
  bimap f g (Vertex n t) = Vertex (g n) (fmap f t)

makeLenses ''Vertex

-- TODO: add position info
data Error s = Error { _state :: !StreamState, _elt :: !s }
  deriving(Eq, Show, Functor, Foldable, Traversable)

makeLenses ''Error

data Token' s n
  = TokenHeader !(GraphHeader s n)
  | TokenEdge !(Edge s n)
  | TokenVertex !(Vertex s n)
  | TokenError !(Error s)
  | TokenEnd
  deriving(Eq, Show, Functor, Foldable, Traversable)

instance Bifunctor Token' where
  bimap f g = \case
    TokenHeader x -> TokenHeader (bimap f g x)
    TokenEdge x   -> TokenEdge (bimap f g x)
    TokenVertex x -> TokenVertex (bimap f g x)
    TokenError x  -> TokenError (fmap f x)
    TokenEnd      -> TokenEnd

makePrisms ''Token'

type Token = Token' Text Text

-- | Parse vertex indices, vertex and edge numbers in tokens
tokenIxsBS :: Token' s ByteString -> Either String (Token' s Int)
tokenIxsBS = sequence . fmap (parseOnly decimal)

tokenIxs :: Token' s Text -> Either String (Token' s Int)
tokenIxs = sequence . fmap (T.parseOnly T.decimal)

-- | Filters correcly parsed tokens and prints malformed ones
report :: Monad m => (b -> m ()) -> ConduitT (Either b a) a m ()
report f = awaitForever $ \case
  Left err -> void $ pure $ f err
  Right token -> yield token

reportError :: (Monad (t IO), MonadTrans t, Show a)
            => ConduitT (Either a b) b (t IO) ()
reportError = report (lift . putStrLn . ("Error: " <>) . show)
