{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
module Data.Matrix.Library(
  BuildError(..),
  _WrongToken, _EmptyMx,
  fromTokens, step,
  toTokens, toTokens',
  toTokensC
  ) where

import Data.Function ((&))
import Data.String   (IsString)

import Control.Lens    (over)
import Control.Lens.TH

import Conduit
import Control.Monad.State.Strict (StateT, get, modify, put)

import Data.Matrix

import ALPS.Graph.Token

-- | Convert 'Matrix Int' to stream of tokens.
-- Returns 'Error' token with empty state and error string,
-- if adjacency matrix is non-square.
-- Its symmetricity is not checked, lower half is dropped
toTokens' :: forall s a i m.
            (IsString s, Monad m)
          => (Int -> Int -> Maybe s)
          -- ^ edge type assigning function
          -> s
          -- ^ graph name
          -> Matrix Bool
          -- ^ adjacency matrix
          -> ConduitT i (Token' s Int) m ()
toTokens' edgeType name mx =
  if nrows mx /= ncols mx
  then yield $ TokenError $ Error SEmpty "toTokens': non-square adajcency matrix"
  else
    let n = nrows mx
        ixMx :: Matrix (Int, Int)
        ixMx = matrix n n id
        mx' :: Matrix (Bool, Token' s Int)
        mx' = elementwise (\(i, j) ok -> (ok, TokenEdge $ Edge (edgeType i j) i j)) ixMx mx
        edges = toList mx' & filter fst & fmap snd
        header = TokenHeader $ GraphHeader name n Nothing -- to not force the list
    in do
      yield header
      yieldMany edges

-- | Make tokens with edges without types
toTokens :: forall s m i.
           (IsString s, Monad m)
           => s
         -> Matrix Bool
         -> ConduitT i (Token' s Int) m ()
toTokens = toTokens' (\_ _ -> Nothing)

-- | Make stream of tokens (without edge types) from stream of matrices,
-- using naming function
toTokensC :: forall s m.
            (IsString s, Monad m )
          => (Int -> s)
          -> ConduitT (Matrix Bool) (Token' s Int) m ()
toTokensC f = evalStateC 1 toTokensC'
  where
    toTokensC' :: ConduitT (Matrix Bool) (Token' s Int) (StateT Int m) ()
    toTokensC' = do
      i <- get
      modify (+1)
      mx <- await
      case mx of
        Just mx -> do
          toTokens (f i) mx
          toTokensC'
        Nothing -> pure ()

-- | Matrix building state
data BuildState mx
  = Empty
  | Allocated !mx

makePrisms ''BuildState

-- | Matrix build error
data BuildError t
  = WrongToken !t
  | EmptyMx !t
  deriving(Eq, Show)

makePrisms ''BuildError

-- | Build stream of fromTokens from stream of edges
fromTokens :: forall m s. Monad m
       => ConduitT
           (Token' s Int)
           (Either (BuildError (Token' s Int)) (Matrix Int))
           m ()
fromTokens = evalStateC Empty step

step :: forall m s. Monad m
     => ConduitT
          (Token' s Int)
          (Either (BuildError (Token' s Int)) (Matrix Int))
          (StateT (BuildState (Matrix Int)) m) ()
step = awaitForever $ \token -> do
  state <- get
  step' state token

step' ::
  Monad m =>
  BuildState (Matrix Int)
  -> Token' s Int
  -> ConduitT
       (Token' s Int)
       (Either (BuildError (Token' s Int)) (Matrix Int))
       (StateT (BuildState (Matrix Int)) m)
       ()
step' Empty (TokenHeader h) = do
  let freshMx n = Allocated $ matrix n n (const 0)
  put $ freshMx $ _graphVertices h
  step

step' (Allocated mx) th@(TokenHeader _) = do
  yield $ Right mx
  step' Empty th

step' (Allocated mx) TokenEnd = do
  yield $ Right mx
  pure ()

step' (Allocated mx) te@(TokenEdge e@(Edge _ i j)) = do
  let n = nrows mx
      validIx k = k > 0 && k <= n
      setEdge (Edge _ i j) = unsafeSet 1 (i, j) . unsafeSet 1 (j, i)
  if validIx i && validIx j
    then do
      modify $ over _Allocated $ setEdge e
      step
    else
      yield $ Left $ WrongToken te

step' _ t = yield $ Left $ WrongToken t
