# alps-graph: streaming parser and renderer for lattice library used in ALPS

[ALPS](http://alps.comp-phys.org/mediawiki/index.php/Main_Page) is a computational toolbox
for strongly correlated quantum mechanical systems.

This package allows to read and write XML files with lattice definitions using [Conduit](https://hackage.haskell.org/package/conduit).
Now only finite lattices are supported.
